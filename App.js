import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AppRegistry, Image } from 'react-native';

export default class App extends React.Component 
{
  render() 
  {
    let pic = 
    {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/a/a2/Ameesha_at_the_Cannes_Film_Festival.jpg'
    };
    return (
      <View style={styles.container1}>
        <View style={styles.container6}>
          <Text style={styles.container3}>Ameesha Patel</Text>
          <Text style={styles.container4}>Actress who made her debut in Kaho Na Pyar Hai</Text>
          <Text>She is an okay actress</Text>
        </View>
        <View style={styles.container7}>
          <Image source={pic} style={styles.container2}/>
        </View>
        
      </View>
      
    );
  }
}

const styles = StyleSheet.create(
  {
  container1: 
  {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  container2:
  {
    width: 293, 
    height: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container3:
  {
    fontSize:40
  },
  container4:
  {
    color: 'red',
    fontSize:25 
  },
  container6:
  {
    flex:2,
    backgroundColor: 'blue'

  },
  container7:
  {
    flex:3,
    backgroundColor: 'green'

  }
});
